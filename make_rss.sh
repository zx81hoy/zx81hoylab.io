#!/usr/bin/env bash

# BashBlog-ng, Bash script to create and manage blogs
# (C) David Satime Wallin <david@dwall.in>, 2019, 2020, 2021, 2022 and contributors
# https://github.com/dvwallin/bashblog-ng/contributors
# (C) Carlos Fenollosa <carlos.fenollosa@gmail.com>, 2011-2016 and contributors
# https://github.com/carlesfe/bashblog/contributors
# Check out README.md for more details

# Generate the feed file
echo -n "Making RSS "

# feed file (rss in this case)
blog_feed="public/feed.xml"
number_of_feed_articles="11" 

global_url='https://zx81hoy.gitlab.io'

datePost=date
rssfile=$blog_feed

{
    echo '<?xml version="1.0" encoding="UTF-8" ?>'
    echo '<rss xmlns:atom="http://www.w3.org/2005/Atom" xmlns:dc="http://purl.org/dc/elements/1.1/" version="2.0">'
    echo "  <channel>"
    echo "      <title>ZX81 hoy</title>"
    echo "      <description>Blog sobre novedades del ordenador Sinclair ZX81</description>"
    echo "      <link>$global_url</link>"
    echo "      <atom:link href=\"$global_url/feed.xml\" rel=\"self\" type=\"application/rss+xml\" />"
    #echo "      <pubDate>$pubdate</pubDate>"
    echo -n "      <pubDate>"
        { cat ./public/js/data.js | grep dateFeed | sed -n 1p | cut -f4 -d"\""; echo -n "/pubDate>"; } | tr "\n" "<"
    echo "" 
    echo -n "      <lastBuildDate>"
        { cat ./public/js/data.js | grep dateFeed | sed -n 1p | cut -f4 -d"\""; echo -n "/lastBuildDate>"; } | tr "\n" "<"
    echo "" 
    #echo "      <lastBuildDate>$pubdate</lastBuildDate>"
    echo "      <language>es</language>"

    n=1
    while IFS='' read -r i; do
        ((n >= number_of_feed_articles)) && break # max 10 items
        echo -n "." 1>&3
        echo '      <item>'
        echo -n '          <title>'
        { cat ./public/js/data.js | grep title | sed -n $((n))p | cut -f4 -d"\""; echo -n '/title>'; } | tr "\n" "<"
        echo ""
        echo -n "          <description><![CDATA["
        { cat <"$i"; echo -n "]]></description>"; } | tr "\n" " "
        echo "" 
        echo -n "          <pubDate>"
        { cat ./public/js/data.js | grep dateFeed | sed -n $((n))p | cut -f4 -d"\""; echo -n "/pubDate>"; } | tr "\n" "<"
        echo "" 
        echo -n "          <link>$global_url/index.html?post="
        { cat ./public/js/data.js | grep href | sed -n $((n))p | cut -f4 -d"\"" ; echo -n "html</link>"; } | tr "\n" "."
        echo ""
        echo -n "          <guid isPermaLink=\"true\">$global_url/index.html?post="
        { cat ./public/js/data.js | grep href | sed -n $((n))p | cut -f4 -d"\"" ; echo -n "html</guid>"; } | tr "\n" "."
        echo ""
        echo "      </item>"
        echo ""
        n=$(( n + 1 ))
    done < <(ls -r ./public/post/*.html)
    
    echo '  </channel>'
    echo '</rss>'
} 3>&1 >"$rssfile"
echo ""

chmod 644 "$blog_feed"
exit 0
