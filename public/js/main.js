  document.onreadystatechange = function (event) {
  if (document.readyState === 'complete') {
    
    /**
    * Add class nameClass to tag element
    * @param {object} element - The tag element.
    * @param {string} nameClass - The name of class.
    */
    var addClass = function(element, nameClass) {
      if (element.classList) {
        element.classList.add(nameClass);
      } else {
        element.nameClass += ' ' + nameClass;
      }
    }

    var escapeRegExp = function(string) {
      return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'); // $& means the whole matched string
    }

    var replaceAll = function(str, match, replacement){
      return str.replace(new RegExp(escapeRegExp(match), 'g'), ()=>replacement);
    }

    /***********************************************************/
    /*******                 TEMPLATE                 **********/
    /***********************************************************/

    var replaceText = function(content, key, value) {
      return replaceAll(content, "{{"+ key +"}}", value ? value : "");
    }

    var replaceTemplate = function(template, object) {
      for(var key in object) {
        template = replaceText(template, key, object[key]);
      }
      return template;
    }

    var autorContainer = document.getElementById('autor-contaniner');
    var tagContainer = document.getElementById('tag-contaniner');
    var aboutContainer = document.getElementById('about');
    var listPost = document.querySelector('#listPost');
    var pagination = document.getElementById('pagination');
    var index = document.getElementById('index');
    var postContainer = document.getElementById('content');
    var pagNow = document.getElementById('pagNow');
    var pagA = document.getElementById('pagA');
    var pagB = document.getElementById('pagB');
    var pagC = document.getElementById('pagC');
    var pagD = document.getElementById('pagD');
    var pagE = document.getElementById('pagE');

    var indexContainer = document.querySelector('#index');
    var aboutLink = document.querySelector('#aboutLink');
    var tagsLink = document.getElementsByClassName('tags');
    var autorsLink = document.getElementsByClassName('autors');
    var postLink = document.getElementsByClassName('post-link-select');
    var paginationLink = document.getElementsByClassName('link-pag');

    var detailsDom =  document.querySelectorAll('details');


    var monthLetter = ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'];
    
    const url = window.location.search;
    const urlParametro = new URLSearchParams(url);
    let post = urlParametro.get("post");
    let page = 0;
    let postXPage = 5;
    let paginationPage = 5; // 1,3,5
    let postListFilter = postList;


    aboutLink.addEventListener('click', function(e) {
      e.preventDefault();
      var request = new XMLHttpRequest();
      request.open('GET', "./page/" + aboutLink.dataset.href + ".html", true);
      request.send(null);
      request.onreadystatechange = function () {
          if (request.readyState === 4 && request.status === 200) {
            aboutContainer.innerHTML = request.responseText;
            aboutContainer.classList.remove('hide');
            addClass(pagination, "hide");
            addClass(aboutLink, "mark");
            addClass(listPost, "hide");
            listPost.innerHTML = '';
            postContainer.innerHTML = '';
            addClass(autorContainer, "hide");
            addClass(tagContainer, "hide");

            window.history.pushState('ZX81 hoy', 'Title', location.href.substring(0,location.href.lastIndexOf('/')) + '/index.html');

            var tagLinkMark = document.getElementsByClassName('tags mark');
            if (tagLinkMark.length > 0) tagLinkMark[0].classList.remove("mark");
            var autorLinkMark = document.getElementsByClassName('autors mark');
            if (autorLinkMark.length > 0) autorLinkMark[0].classList.remove("mark");
            var postLinkMark = document.getElementsByClassName('name-sidebar mark');
            if (postLinkMark.length > 0) postLinkMark[0].classList.remove("mark");

            detailsDom =  document.querySelectorAll('details');
            var detailsDomArray = Array.from(detailsDom);
            detailsDomArray.forEach(function(detailsItem) {detailsItem.open = false });
          }
      }
    });

    var loadPost = function(postContent, href, changeURL) {
      document.getElementById('content').innerHTML = postContent;

      if (changeURL) {
        window.history.pushState('ZX81 hoy', 'Title', location.href.substring(0,location.href.lastIndexOf('/')) + '/index.html?post='+href);
      }

      postContainer.classList.remove('hide');
      addClass(pagination, "hide");

      var tagLinkMark = document.getElementsByClassName('tags mark');
      if (tagLinkMark.length > 0) tagLinkMark[0].classList.remove("mark");
      var autorLinkMark = document.getElementsByClassName('autors mark');
      if (autorLinkMark.length > 0) autorLinkMark[0].classList.remove("mark");
      var postLinkMark = document.getElementsByClassName('name-sidebar mark');
      if (postLinkMark.length > 0) postLinkMark[0].classList.remove("mark");

      var year = href.substring(0,2);
      var month = href.substring(3,5);
      var day = href.substring(6,8);

      detailsDom =  document.querySelectorAll('details');
      var detailsDomArray = Array.from(detailsDom);
      detailsDomArray.forEach(function(detailsItem) { detailsItem.open = false });

      document.querySelector('#year-20'+year).open = true;
      month = monthLetter[parseInt(month)-1]; // mapping 02 -> Feb
      document.querySelector('#y-20'+year+"_m-"+month).open = true;
      var dayLink = document.querySelector('#y-20'+year+"_m-"+month+"_d-"+day);
      addClass(dayLink, "mark");

      autorLinkAddEvent();
      tagLinkAddEvent();
    };

    var postLinkAddEvent = function(postList) {
      var postLinkArray = Array.from(postList);
      postLinkArray.forEach(function(post) {
        post.addEventListener('click', function(e) {
          aboutContainer.innerHTML = '';
          aboutLink.classList.remove("mark");    
          addClass(aboutContainer,'hide');
          addClass(tagContainer,'hide');
          addClass(autorContainer,'hide');
          listPost.innerHTML = '';
          addClass(listPost,'hide');

          e.preventDefault();

          var request = new XMLHttpRequest();

          request.addEventListener("load", (evt) => {
            if (evt.target.status !== 200) {              
              setTimeout(() => {
                request.open('GET', "./post/"+post.dataset.href+".html", true);
                request.send(null);
              }, 10000); 
            }
          });

          request.open('GET', "./post/"+post.dataset.href+".html", true);
          request.send(null);
          request.onreadystatechange = function () {
            if (request.readyState === 4 && request.status === 200) {
              loadPost(request.responseText, post.dataset.href, true);
            }
          }

        });
      });
    }

    var autorLinkAddEvent = function() {
    var autorsLinkArray = Array.from(autorsLink);
      autorsLinkArray.forEach(function(autor) {
        autor.addEventListener('click', function(e) {
          aboutLink.classList.remove("mark");        
          aboutContainer.innerHTML = '';
          addClass(aboutContainer,'hide');
          addClass(tagContainer,'hide');
          addClass(postContainer,'hide');

          listPost.classList.remove('hide');

          e.preventDefault();
          postListFilter = postList.filter(post => post.autor.includes(autor.dataset.href));
          autorContainer.classList.remove('hide');
          document.querySelector('#autor').innerHTML = autor.dataset.href;

          //window.history.pushState('ZX81 hoy', 'Title', '/index.html');
          window.history.pushState('ZX81 hoy', 'Title', location.href.substring(0,location.href.lastIndexOf('/')) + '/index.html');
          
          addClass(pagination, "hide");


          detailsDom =  document.querySelectorAll('details');
          var detailsDomArray = Array.from(detailsDom);
          detailsDomArray.forEach(function(detailsItem) { detailsItem.open = false });

          document.querySelector('#autorSideBar').open = true;

          var tagLinkMark = document.getElementsByClassName('tags mark');
          if (tagLinkMark.length > 0) tagLinkMark[0].classList.remove("mark");
          var autorLinkMark = document.getElementsByClassName('autors mark');
          if (autorLinkMark.length > 0) autorLinkMark[0].classList.remove("mark");

          var postLinkMark = document.getElementsByClassName('name-sidebar mark');
          if (postLinkMark.length > 0) postLinkMark[0].classList.remove("mark");

          var autorLink = document.querySelector('#autor_'+autor.dataset.href.replace(' ','_'));
          addClass(autorLink, "mark");
          //todo desmarcar fecha
          renderList(false);
        });
      })
    }

    var tagLinkAddEvent = function() {
      var tagsLinkArray = Array.from(tagsLink);
      tagsLinkArray.forEach(function(tag) {
        tag.addEventListener('click', function(e) {
          aboutLink.classList.remove("mark");        
          aboutContainer.innerHTML = '';
          addClass(aboutContainer,'hide');
          addClass(autorContainer,'hide');
          addClass(postContainer,'hide');

          listPost.classList.remove('hide');

          e.preventDefault();

          location.href.substring(0,location.href.lastIndexOf('/'))

          window.history.pushState('ZX81 hoy', 'Title', location.href.substring(0,location.href.lastIndexOf('/')) + '/index.html');
          postListFilter = postList.filter(post => post.tags.includes(tag.dataset.href));
          tagContainer.classList.remove('hide');
          document.querySelector('#tag').innerHTML = tag.dataset.href;
          addClass(pagination, "hide");


          detailsDom =  document.querySelectorAll('details');
          var detailsDomArray = Array.from(detailsDom);
          detailsDomArray.forEach(function(detailsItem) {detailsItem.open = false });

          document.querySelector('#tagSideBar').open = true;

          var tagLinkMark = document.getElementsByClassName('tags mark');
          if (tagLinkMark.length > 0) tagLinkMark[0].classList.remove("mark");
          var autorLinkMark = document.getElementsByClassName('autors mark');
          if (autorLinkMark.length > 0) autorLinkMark[0].classList.remove("mark");

          var postLinkMark = document.getElementsByClassName('name-sidebar mark');
          if (postLinkMark.length > 0) postLinkMark[0].classList.remove("mark");

          var tagLink = document.querySelector('#tag_'+tag.dataset.href);
          addClass(tagLink, "mark");
          renderList(false);
        });
      });
    }

    var reload = function() {
      window.history.pushState('ZX81 hoy', 'Title', location.href.substring(0,location.href.lastIndexOf('/')) + '/index.html');  
      listPost.innerHTML = '';
      aboutLink.classList.remove("mark");   
      aboutContainer.innerHTML = '';
      addClass(aboutContainer,'hide');
      addClass(tagContainer,'hide');
      addClass(postContainer,'hide');
      addClass(autorContainer, "hide");

      listPost.classList.remove('hide');

      postListFilter = postList;

      pagination.classList.remove("hide");

      detailsDom =  document.querySelectorAll('details');
      var detailsDomArray = Array.from(detailsDom);
      detailsDomArray.forEach(function(detailsItem) { detailsItem.open = false });

      var tagLinkMark = document.getElementsByClassName('tags mark');
      if (tagLinkMark.length > 0) tagLinkMark[0].classList.remove("mark");
      var autorLinkMark = document.getElementsByClassName('autors mark');
      if (autorLinkMark.length > 0) autorLinkMark[0].classList.remove("mark");
      var postLinkMark = document.getElementsByClassName('name-sidebar mark');
      if (postLinkMark.length > 0) postLinkMark[0].classList.remove("mark");
      renderList(true);
    }

    var paginationLinkAddEvent = function() {
      var paginationLinkArray = Array.from(paginationLink);
      paginationLinkArray.forEach(function(pagLink) {
        pagLink.addEventListener('click', function(e) {
          e.preventDefault();
          page = this.dataset.page;
          reload();
        });
      });
    }

    var renderList = function(printPagination) {

      listPost.innerHTML = '';

      if (page == undefined || page == null || page < 0) {
        page = 0;
      }

      let postStart = parseInt(page) * postXPage;
      let postEnd = (parseInt(page) + 1) * postXPage;
      if (printPagination) {
        postListFilter = postListFilter.slice(postStart, postEnd);
      }

      /*    Render template     */
      postListFilter.forEach(function(post) {
        var template = document.getElementById("listPostTemplate").innerHTML;

        template = replaceTemplate(template, {
          "href": post.href,
          "asset": post.asset,
          "title": post.title.substring(0,50),
          "date": post.date,
          "dateFormat": post.dateFormat
        });

        var el = document.createElement('li');
        
        el.innerHTML = template;

        listPost.appendChild(el);

      });

      if (printPagination) {

        /***********************************************************/
        /*******                Pagination                **********/
        /***********************************************************/

        var pagPrev = document.getElementById('pagPrev');
        var pagNext = document.getElementById('pagNext');

        var pagInit = document.getElementById('pagInit');
        var pagEnd = document.getElementById('pagEnd');
        if (page == 0) {
          addClass(pagInit, "disabled");
          addClass(pagPrev, "disabled");
        }
        else {
          pagInit.classList.remove("disabled");
          pagPrev.dataset.page = (parseInt(page) - 1);
          pagPrev.classList.remove("disabled");          
        }

        pagEnd.dataset.page = parseInt(postList.length/postXPage) - 1;
        if (postList.length%postXPage > 0) {
          pagEnd.dataset.page++;
        }
        pagEnd.classList.remove("disabled");

        pagNext.dataset.page = (parseInt(page) + 1);
        pagNext.classList.remove("disabled");

        if (postEnd >= postList.length) {
          addClass(pagNext, "disabled");
          addClass(pagEnd, "disabled");
        }

        if (paginationPage == 1) {
          pagA.innerHTML = parseInt(page) + 1;
          addClass(pagA, "disabled");
          addClass(pagB, "hide");
          addClass(pagC, "hide");
          addClass(pagD, "hide");
          addClass(pagE, "hide");
        } else if (paginationPage == 3) {
          addClass(pagD, "hide");
          addClass(pagE, "hide");
          if (page == 0) {
            pagA.innerHTML = parseInt(page) + 1;
            addClass(pagA, "disabled");
            pagB.innerHTML = parseInt(page) + 2;
            pagB.classList.remove("disabled");
            pagC.innerHTML = parseInt(page) + 3;
            pagC.classList.remove("disabled");

            pagB.dataset.page = (parseInt(page) + 1);
            pagC.dataset.page = (parseInt(page) + 2);
          } else if (page > 0 && postList.length >= postEnd + 1) {
            pagA.innerHTML = parseInt(page);
            pagA.classList.remove("disabled");
            pagB.innerHTML = parseInt(page) + 1;
            addClass(pagB, "disabled");
            pagC.innerHTML = parseInt(page) + 2;
            pagC.classList.remove("disabled");

            pagA.dataset.page = (parseInt(page) - 1);
            pagC.dataset.page = (parseInt(page) + 1);
          } else {
            pagA.innerHTML = parseInt(page) - 1;
            pagA.classList.remove("disabled");
            pagB.innerHTML = parseInt(page);
            pagB.classList.remove("disabled");
            pagC.innerHTML = parseInt(page) + 1;
            addClass(pagC, "disabled");

            pagA.dataset.page = (parseInt(page) - 2);
            pagB.dataset.page = (parseInt(page) - 1);
          }
        } else {
          if (page == 0) {
            pagA.innerHTML = parseInt(page) + 1;
            addClass(pagA, "disabled");
            pagB.innerHTML = parseInt(page) + 2;
            pagB.classList.remove("disabled");
            pagC.innerHTML = parseInt(page) + 3;
            pagC.classList.remove("disabled");
            pagD.innerHTML = parseInt(page) + 4;
            pagD.classList.remove("disabled");
            pagE.innerHTML = parseInt(page) + 5;
            pagE.classList.remove("disabled");

            pagB.dataset.page = (parseInt(page) +1);
            pagC.dataset.page = (parseInt(page) +2);
            pagD.dataset.page = (parseInt(page) +3);
            pagE.dataset.page = (parseInt(page) +4);
          } else if (page == 1) {
            pagA.innerHTML = parseInt(page);
            pagA.classList.remove("disabled");
            pagB.innerHTML = parseInt(page) + 1;
            addClass(pagB, "disabled");
            pagC.innerHTML = parseInt(page) + 2;
            pagC.classList.remove("disabled");
            pagD.innerHTML = parseInt(page) + 3;
            pagD.classList.remove("disabled");
            pagE.innerHTML = parseInt(page) + 4;
            pagE.classList.remove("disabled");

            pagA.dataset.page = (parseInt(page) -1);
            pagC.dataset.page = (parseInt(page) +1);
            pagD.dataset.page = (parseInt(page) +2);
            pagE.dataset.page = (parseInt(page) +3);
          } else if (page == 2 || (page > 2 && postList.length >= postEnd + postXPage +1) ) {
            pagA.innerHTML = parseInt(page) - 1;
            pagA.classList.remove("disabled");
            pagB.innerHTML = parseInt(page);
            pagB.classList.remove("disabled");
            pagC.innerHTML = parseInt(page) + 1;
            addClass(pagC, "disabled");
            pagD.innerHTML = parseInt(page) + 2;
            pagD.classList.remove("disabled");
            pagE.innerHTML = parseInt(page) + 3;
            pagE.classList.remove("disabled");

            pagA.dataset.page = (parseInt(page) -2);
            pagB.dataset.page = (parseInt(page) -1);
            pagD.dataset.page = (parseInt(page) +1);
            pagE.dataset.page = (parseInt(page) +2);
          } else if (page > 2 && postList.length >= postEnd + 1) {
            pagA.innerHTML = parseInt(page) - 2;
            pagA.classList.remove("disabled");
            pagB.innerHTML = parseInt(page) - 1;
            pagB.classList.remove("disabled");
            pagC.innerHTML = parseInt(page);
            pagC.classList.remove("disabled");
            pagD.innerHTML = parseInt(page) + 1;
            addClass(pagD, "disabled");
            pagE.innerHTML = parseInt(page) + 2;
            pagE.classList.remove("disabled");

            pagA.dataset.page = (parseInt(page) -3);
            pagB.dataset.page = (parseInt(page) -2);
            pagC.dataset.page = (parseInt(page) -1);
            pagE.dataset.page = (parseInt(page) +1);
          } else {
            pagA.innerHTML = parseInt(page) - 3;
            pagA.classList.remove("disabled");
            pagB.innerHTML = parseInt(page) - 2;
            pagB.classList.remove("disabled");
            pagC.innerHTML = parseInt(page) - 1;
            pagC.classList.remove("disabled");
            pagD.innerHTML = parseInt(page);
            pagD.classList.remove("disabled");
            pagE.innerHTML = parseInt(page) + 1;
            addClass(pagE, "disabled");

            pagA.dataset.page = (parseInt(page) -4);
            pagB.dataset.page = (parseInt(page) -3);
            pagC.dataset.page = (parseInt(page) -2);
            pagD.dataset.page = (parseInt(page) -1);
          }
        }
      }

        postLinkAddEvent(document.getElementsByClassName('post-link-list-select'));
    }

    if (post == null || post == undefined) {
      renderList(true);
    } else {
      aboutContainer.innerHTML = '';
      aboutLink.classList.remove("mark");    
      addClass(aboutContainer,'hide');
      addClass(tagContainer,'hide');
      addClass(autorContainer,'hide');
      listPost.innerHTML = '';
      addClass(listPost,'hide');

      var request = new XMLHttpRequest();
      request.open('GET', "./post/"+post+".html", true);
      request.send(null);
      request.onreadystatechange = function () {
        if (request.readyState === 4 && request.status === 200) {
          loadPost(request.responseText, post, false);
        }
      }
    }


    /*    Render sidebar     */
    autorList.forEach(function(autor) {
      var autorTemplate = document.getElementById("autorTemplate").innerHTML;

      autorTemplate = replaceTemplate(autorTemplate, {
        "autor": autor.autor,
        "autor_format": autor.autor.replace(' ','_'),
        "numPost": autor.numPost
      });

      var el = document.createElement('li');
      addClass(el, "name-sidebar");        
      el.innerHTML = autorTemplate;

      document.querySelector('#autorListContainer').appendChild(el);
    });
    
    tagList.forEach(function(tag) {
      var autorTemplate = document.getElementById("tagTemplate").innerHTML;

      autorTemplate = replaceTemplate(autorTemplate, {
        "name": tag.name,
        "numPost": tag.numPost
      });

      var el = document.createElement('li');
      addClass(el, "name-sidebar");        
      el.innerHTML = autorTemplate;

      document.querySelector('#tagListContainer').appendChild(el);
    });

    dateList.forEach(function(datePosts) {
      var yearTemplate = document.getElementById("yearTemplate").innerHTML;

      yearTemplate = replaceTemplate(yearTemplate, {
        "year": datePosts.year,
        "yearNum": datePosts.yearNum
      });

      var el = document.createElement('details');
      el.id= "year-" + datePosts.year;
      el.innerHTML = yearTemplate;


      //foreach months
      datePosts.months.forEach(function(monthPosts) {
        var monthTemplate = document.getElementById("monthTemplate").innerHTML;

        monthTemplate = replaceTemplate(monthTemplate, {
          "month": monthPosts.month,
          "monthNum": monthPosts.post.length,
          "monthId": "y-" + datePosts.year + "_m-" + monthPosts.month
        });

        var elMonth = document.createElement('details');
        elMonth.id= "y-" + datePosts.year + "_m-" + monthPosts.month;
        elMonth.innerHTML = monthTemplate;
        el.appendChild(elMonth);
      
        //foreach days
        monthPosts.post.forEach(function(dayPosts) {
          var dayTemplate = document.getElementById("dayTemplate").innerHTML;

          dayTemplate = replaceTemplate(dayTemplate, {
            "day": dayPosts.day,
            "title": dayPosts.title,
            "href": dayPosts.href
          });

          var elDay = document.createElement('li');
          addClass(elDay,"name-sidebar");
          elDay.id= "y-" + datePosts.year + "_m-" + monthPosts.month+ "_d-" + dayPosts.day;
          elDay.innerHTML = dayTemplate;

          elMonth.querySelector('#'+"y-" + datePosts.year + "_m-" + monthPosts.month).appendChild(elDay);
        });
      });      

      document.querySelector('#dateSidebar').appendChild(el);
    });

    indexContainer.addEventListener('click', function(e) {
      page = 0;      
      e.preventDefault();
      reload();
    });

    postLinkAddEvent(document.getElementsByClassName('post-link-select'));

    tagsLink = document.getElementsByClassName('tags');
    autorsLink = document.getElementsByClassName('autors');
    autorLinkAddEvent();
    tagLinkAddEvent();
    paginationLinkAddEvent();

}
  

}
  
